#-------------------------------------------------
#
# Project created by QtCreator 2018-05-26T14:54:07
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Guides
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include( ../src/src.pri )

mac {
  ICON = ../resources/icons/guides.icns
}
win32 {
  RC_ICONS = ../resources/icons/guides.ico
}
