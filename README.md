# Guides

![Screenshot](images/screenshot1.png "Screenshot")

## Overview

Overlay bounding boxes for aligning or measuring onscreen items with pixel perfect accuracy.

### Features

- Multiple guides per session.
- Change line color and opacity.
- Enable line dashes.
- Dim the surrounding regions.
- Run on any platform where Qt can be built.
- Save and restore session with JSON file.
- Hidable grips to move and resize.
- Granular (micro) grip mouse movement.

## Building

    cd ide
    ./build
    ./install
    make distclean

## License

Do Whatever You Want License (C) 2018 Remik Ziemlinski

