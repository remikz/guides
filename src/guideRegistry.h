#pragma once

#include <QMap>

class Guide;

class GuideRegistry
{
public:
  ~GuideRegistry();

  static GuideRegistry* instance();

  Guide* add();
  void add(Guide* guide);
  void clear();
  Guide* get(int id);
  int  lastId();
  void remove(int id);

  const QMap<int,Guide*> & getMap() const;

private:
  GuideRegistry();

  QMap<int,Guide*> mIdToGuide;
};
