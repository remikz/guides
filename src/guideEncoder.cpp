#include "guide.h"
#include "guideEncoder.h"
#include "guideRegistry.h"

#define KEY_BOTTOM "bottom"
#define KEY_COLOR "color"
#define KEY_DASH "dash"
#define KEY_DIM "dim"
#define KEY_GUIDES "guides"
#define KEY_ID "id"
#define KEY_LEFT "left"
#define KEY_OPACITY "opacity"
#define KEY_RIGHT "right"
#define KEY_TOP "top"
#define KEY_VERSION "version"
#define KEY_VISIBLE "visible"

//============================================================
QVariantMap GuideEncoder::encode()
{
  QVariantList guides;

  const auto & map = GuideRegistry::instance()->getMap();
  const auto end   = map.end();

  for( auto iter = map.begin(); iter != end; ++iter )
  {
    auto guide = iter.value();
    guides << encodeGuide( guide );
  }

  QVariantMap result;
  result.insert( KEY_VERSION, 1 );
  result.insert( KEY_GUIDES, guides );

  return result;
}
//============================================================
QVariantMap GuideEncoder::encodeGuide(const Guide* guide)
{
  QVariantMap result;
  result.insert( KEY_ID, guide->id );
  result.insert( KEY_BOTTOM, guide->bottom );
  result.insert( KEY_DASH, guide->dash );
  result.insert( KEY_DIM, guide->dim );
  result.insert( KEY_LEFT, guide->left );
  result.insert( KEY_OPACITY, guide->opacity );
  result.insert( KEY_RIGHT, guide->right );
  result.insert( KEY_TOP, guide->top );
  result.insert( KEY_VISIBLE, guide->visible );

  QVariantList color;
  color << guide->color.red()
        << guide->color.green()
        << guide->color.blue();
  result.insert( KEY_COLOR, color );

  return result;
}
//============================================================
void GuideEncoder::decode(const QVariantMap & json)
{
  if( json.value( KEY_VERSION ) != "1" )
    return;

  auto registry = GuideRegistry::instance();
  registry->clear();

  auto maps = json.value( KEY_GUIDES ).toList();

  for( QVariant & item: maps )
  {
    auto map = item.toMap();
    Guide* guide = new Guide;

    QVariantList color = map.value( KEY_COLOR ).toList();
    guide->color.setRed( color[0].toInt() );
    guide->color.setGreen( color[1].toInt() );
    guide->color.setBlue( color[2].toInt() );

    guide->bottom = map.value( KEY_BOTTOM ).toInt();
    guide->dash = map.value( KEY_DASH ).toInt();
    guide->dim = map.value( KEY_DIM ).toInt();
    guide->id = map.value( KEY_ID ).toInt();
    guide->left = map.value( KEY_LEFT ).toInt();
    guide->opacity = map.value( KEY_OPACITY ).toInt();
    guide->right = map.value( KEY_RIGHT ).toInt();
    guide->top = map.value( KEY_TOP ).toInt();
    guide->visible = map.value( KEY_VISIBLE ).toBool();

    registry->add( guide );
  }
}
//============================================================
