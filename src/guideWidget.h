#pragma once

#include <QWidget>

#include "guide.h"

class GuideRegistry;

class GuideWidget : public QWidget
{
  Q_OBJECT
public:
  GuideWidget(QWidget* parent=nullptr);
  ~GuideWidget();

signals:
  void dimChangedSignal(int value);

protected:
  void paintEvent(QPaintEvent *event) override;
  void paintGuide(QPainter & painter, Guide* guide);

private:
  GuideRegistry* mRegistry;
};
