#include <QDebug>
#include <QKeyEvent>

#include "spinBox.h"

//============================================================
void SpinBox::keyPressEvent(QKeyEvent* event)
{
  int dir = 1;
  int delta = 1;
  if( event->modifiers() & Qt::ShiftModifier )
  {
    delta = 10;
  }
  else if( event->modifiers() & Qt::ControlModifier )
  {
    delta = 100;
  }

  switch( event->key() )
  {
    case Qt::Key_Down:
    case Qt::Key_Left:
      dir = -1;
      break;
    case Qt::Key_Shift:
    case Qt::Key_Control:
      QSpinBox::keyPressEvent( event );
      return;
    default:
      break;
  }

  if( 0 == event->count() ||
      1 == delta )
  {
    QSpinBox::keyPressEvent( event );
    return;
  }

  event->accept();
  setValue( value() + dir * delta );
}
//============================================================
