#include <QDebug>
#include <QGridLayout>
#include <QLabel>
#include <QKeyEvent>
#include <QPushButton>

#include <QDesktopWidget>

#include "gripWidget.h"
#include "guideWidget.h"
#include "guideWindow.h"

//============================================================
GuideWindow::GuideWindow(GuideWidget* guide,
                         QWidget *parent) :
  QMainWindow( parent ),
  mGuide( guide )
{
  setWindowFlags( Qt::FramelessWindowHint |
                  Qt::WindowStaysOnTopHint |
                  Qt::NoDropShadowWindowHint |
                  Qt::WindowTransparentForInput );
  setAttribute( Qt::WA_NoSystemBackground, true );
  setAttribute( Qt::WA_TranslucentBackground, true );
  setAttribute( Qt::WA_TransparentForMouseEvents );
  setFocusPolicy( Qt::NoFocus );

  move( 0, 0 );
  QDesktopWidget desktop;
  resize( desktop.screenGeometry( desktop.screenNumber( this ) ).size() );

  auto container = new QWidget;
  auto grid = new QGridLayout( container );
  grid->setSpacing( 0 );
  grid->setMargin( 0 );
  grid->addWidget( guide );
  setCentralWidget( container );
}
//============================================================
