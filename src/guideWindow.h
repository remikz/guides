#pragma once

#include <QMainWindow>

class GripWidget;
class GuideWidget;

class GuideWindow : public QMainWindow
{
  Q_OBJECT
public:
  GuideWindow(GuideWidget* guide,
              QWidget *parent=nullptr);

private:
  GuideWidget* mGuide;
};
