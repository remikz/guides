#pragma once

#include <QMainWindow>

class ControlWidget;
class GripWidget;
class GuideFile;
class QAction;

class MainWindow : public QMainWindow
{
  Q_OBJECT
public:
  MainWindow();
  ~MainWindow();

  void setControlWidget(ControlWidget* w);
  void setGripWidget(GripWidget* grip);

signals:
  void showGripsSignal(bool show);

protected slots:
  void newGuide();
  void openFile();
  void saveFile();
  void saveAsFile();
  void closeGuide();

  void gripActionChanged();

protected:
  void closeEvent(QCloseEvent *event) override;

  void init();
  void createMenus();
  void updateMenus();

private:
  QAction* mCloseAction;
  ControlWidget* mControlWidget;
  GuideFile* mFile;
  QAction* mGripAction;
  GripWidget* mGripWidget;
};
