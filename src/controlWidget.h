#pragma once

#include <QWidget>

//Forward declarations
class Guide;
class GuideRegistry;
class GuideWidget;
class QCheckBox;
class QComboBox;
class QPushButton;
class QSpinBox;

class ControlWidget : public QWidget
{
  Q_OBJECT
public:
  ControlWidget(GuideWidget* guide, QWidget* parent=nullptr);
  ~ControlWidget();

  void addNewGuide();
  void removeCurrentGuide();
  void setCurrentGuide(int id, bool updateWidgets);
  int currentGuideId() const;
  Guide* currentGuide() const;
  void loadRegistry();

signals:
  void guideChangedSignal(int id);

public slots:
  void guideMovedSlot(int id);
  void guideResizedSlot(int id);

protected slots:
  void bottomValueChangedSlot(int v);
  void colorClicked();
  void dashValueChangedSlot(int v);
  void dimValueChangedSlot(int v);
  void heightValueChangedSlot(int v);
  void idValueChangedSlot(const QString & text);
  void leftValueChangedSlot(int v);
  void opacityValueChangedSlot(int v);
  void rightValueChangedSlot(int v);
  void topValueChangedSlot(int v);
  void visibleChangedSlot(int state);
  void widthValueChangedSlot(int v);

protected:
  void emitGuideChangedSignal();

  int getHeight() const;
  int getWidth() const;

  void init();
  void initWidgets();
  void initLayout();
  void initSignals();
  void initDefaults();

  void restoreWidgets();

  void setColor(QColor c);

  void keyPressEvent(QKeyEvent *event) override;

private:
  QSpinBox *mBottom, *mDash, *mDim, *mHeight, *mLeft, *mOpacity, *mRight, *mTop, *mWidth;
  QPushButton *mColor;
  GuideWidget* mGuideWidget;
  QCheckBox* mVisible;
  QComboBox* mIds;
  GuideRegistry* mRegistry;
};
