#include <QApplication>
#include "controlWidget.h"
#include "gripWidget.h"
#include "gripWindow.h"
#include "guideWidget.h"
#include "guideWindow.h"
#include "mainWindow.h"

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  MainWindow mainWin;
  mainWin.setWindowTitle( "Guides" );
  mainWin.show();
  mainWin.move( 100,100 );

  auto guide = new GuideWidget;
  auto guideWin = new GuideWindow( guide );
  guideWin->show();

  auto grip = new GripWidget( guide );
  mainWin.connect( & mainWin, & MainWindow::showGripsSignal, grip, & GripWidget::setVisible );
  auto gripWin = new GripWindow( grip );
  gripWin->show();

  auto control = new ControlWidget( guide );
  control->connect( grip,    & GripWidget::guideMovedSignal,
                    control, & ControlWidget::guideMovedSlot );
  control->connect( grip,    & GripWidget::guideResizedSignal,
                    control, & ControlWidget::guideResizedSlot );
  control->connect( control, & ControlWidget::guideChangedSignal,
                    grip,    & GripWidget::guideChangedSlot );
  control->addNewGuide();
  mainWin.setControlWidget( control );
  mainWin.setGripWidget( grip );

  return a.exec();
}
