#include <QDebug>
#include <QDesktopWidget>
#include <QPainter>
#include <QPaintEvent>

#include "guideRegistry.h"
#include "guideWidget.h"

//============================================================
GuideWidget::GuideWidget(QWidget* parent) :
  QWidget( parent )
{
  mRegistry = GuideRegistry::instance();
}
//============================================================
GuideWidget::~GuideWidget()
{
}
//============================================================
void GuideWidget::paintEvent(QPaintEvent* /* event */)
{
  const auto & map = mRegistry->getMap();
  const auto end   = map.end();

  QPainter painter( this );

  for( auto iter = map.begin(); iter != end; ++iter )
  {
    auto guide = iter.value();
    paintGuide( painter, guide );
  }
}
//============================================================
void GuideWidget::paintGuide(QPainter & painter, Guide* guide)
{
  if( !guide ||
      !guide->visible )
    return;

  auto color = guide->color;
  color.setAlpha( guide->opacity );

  QPen pen( color, Qt::FlatCap );
  if( guide->dash )
  {
    pen.setDashPattern( QVector<qreal>({ (qreal) guide->dash, (qreal) guide->dash }) );
  }

  painter.setPen( pen );

  painter.drawLine( guide->left,  guide->top,    guide->left,  guide->bottom );
  painter.drawLine( guide->left,  guide->top,    guide->right, guide->top );
  painter.drawLine( guide->right, guide->top,    guide->right, guide->bottom );
  painter.drawLine( guide->left,  guide->bottom, guide->right, guide->bottom );

  if( guide->dim < 255 )
  {
    QDesktopWidget desktop;
    auto geom =  desktop.screenGeometry( desktop.screenNumber( this ) );
    QColor dim( 0, 0, 0, 255 - guide->dim );

    painter.fillRect( 0, 0, geom.width(),
                      guide->top, dim );
    painter.fillRect( 0, guide->top, guide->left,
                      guide->bottom - guide->top + 1, dim );
    painter.fillRect( 0, guide->bottom+1, geom.width(),
                      geom.height() - guide->bottom, dim );
    painter.fillRect( guide->right+1, guide->top,
                      geom.width() - guide->left, guide->bottom - guide->top + 1, dim );
  }
}
//============================================================
