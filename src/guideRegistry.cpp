#include "guideRegistry.h"
#include "guide.h"

//============================================================
GuideRegistry::GuideRegistry()
{
}
//============================================================
GuideRegistry::~GuideRegistry()
{
  clear();
}
//============================================================
const QMap<int,Guide*> & GuideRegistry::getMap() const { return mIdToGuide; }
//============================================================
GuideRegistry* GuideRegistry::instance()
{
  static GuideRegistry r;
  return & r;
}
//============================================================
Guide* GuideRegistry::add()
{
  auto g = new Guide;
  if( mIdToGuide.isEmpty() )
  {
    g->id = 1;
  }
  else
  {
    g->id = lastId() + 1;
  }

  mIdToGuide.insert( g->id, g );

  return g;
}
//============================================================
void GuideRegistry::add(Guide* guide)
{
  mIdToGuide.insert( guide->id, guide );
}
//============================================================
void GuideRegistry::clear()
{
  qDeleteAll( mIdToGuide.values() );
  mIdToGuide.clear();
}
//============================================================
Guide* GuideRegistry::get(int id)
{
  return mIdToGuide.value( id );
}
//============================================================
int GuideRegistry::lastId()
{
  if( mIdToGuide.isEmpty() )
    return -1;

  return mIdToGuide.lastKey();
}
//============================================================
void GuideRegistry::remove(int id)
{
  if( !mIdToGuide.contains( id ) )
    return;

  auto g = mIdToGuide.take( id );
  delete g;
}
//============================================================
