#include "guide.h"

//============================================================
Guide::Guide() :
  color( Qt::red ),
  bottom( 0 ),
  dash( 10 ),
  dim( 255 ),
  id( -1 ),
  left( 0 ),
  opacity( 255 ),
  right( 0 ),
  top( 0 ),
  visible( true )
{
}
//============================================================
int Guide::height() const { return bottom - top + 1; }
int Guide::width() const { return right - left + 1; }
//============================================================
QRect Guide::rect() const
{
  return QRect( left, top, width(), height() );
}
//============================================================
QRegion Guide::region() const
{
  QRegion result;

  if( dim < 255 )
  {
    result += QRect( qMin( left, right ) - 1,
                     qMin( top, bottom ) - 1,
                     qAbs( width() ) + 2,
                     qAbs( height() ) + 2 );
  }
  else
  {
    int h, w;
    if( bottom >= top )
    {
      h = bottom - top + 2;
      result += QRect( left-1,  top-1, 2, h );
      result += QRect( right-1, top-1, 2, h );
    }
    else
    {
      h = top - bottom + 2;
      result += QRect( left-1,  bottom-1, 2, h );
      result += QRect( right-1, bottom-1, 2, h );
    }

    if( right >= left )
    {
      w = right - left + 2;
      result += QRect( left-1, top-1,    w, 2 );
      result += QRect( left-1, bottom-1, w, 2 );
    }
    else
    {
      w = left - right + 2;
      result += QRect( right-1, top-1,    w, 2 );
      result += QRect( right-1, bottom-1, w, 2 );
    }
  }

  return result;
}
//============================================================
