#pragma once

#include <QColor>
#include <QRect>
#include <QRegion>

class Guide
{
public:
  Guide();

  int height() const;
  int width() const;
  QRect rect() const;
  QRegion region() const;

  QColor color;
  int bottom, dash, dim, id, left, opacity, right, top;
  bool visible;
};
