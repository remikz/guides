#include <QApplication>
#include <QAction>
#include <QMenu>
#include <QMenuBar>

#include "controlWidget.h"
#include "gripWidget.h"
#include "guideFile.h"
#include "guideRegistry.h"
#include "mainWindow.h"

//============================================================
MainWindow::MainWindow() :
  mControlWidget( nullptr ),
  mGripWidget( nullptr )
{
  init();
}
//============================================================
MainWindow::~MainWindow()
{
  delete mFile;
}
//============================================================
void MainWindow::init()
{
  mFile = new GuideFile;
  createMenus();
  updateMenus();
}
//============================================================
void MainWindow::createMenus()
{
  auto newAct = new QAction( tr( "&New Guide" ), this );
  newAct->setShortcuts( QKeySequence::New );
  newAct->setStatusTip( tr( "Create a new guide" ) );
  connect( newAct, & QAction::triggered, this, & MainWindow::newGuide );

  mCloseAction = new QAction( tr( "&Close Guide" ), this );
  mCloseAction->setShortcuts( QKeySequence::Close );
  mCloseAction->setStatusTip( tr( "Close current guide" ) );
  connect( mCloseAction, & QAction::triggered, this, & MainWindow::closeGuide );

  auto openAct = new QAction( tr( "&Open..." ), this );
  openAct->setShortcuts( QKeySequence::Open );
  openAct->setStatusTip( tr( "Open a guide file" ) );
  connect( openAct, & QAction::triggered, this, & MainWindow::openFile );

  auto saveAct = new QAction( tr( "&Save" ), this );
  saveAct->setShortcuts( QKeySequence::Save );
  saveAct->setStatusTip( tr( "Save guides to file" ) );
  connect( saveAct, & QAction::triggered, this, & MainWindow::saveFile );

  auto saveAsAct = new QAction( tr( "Save &As..." ), this );
  saveAsAct->setShortcuts( QKeySequence::SaveAs );
  saveAsAct->setStatusTip( tr( "Save guides to another file" ) );
  connect( saveAsAct, & QAction::triggered, this, & MainWindow::saveAsFile );

  auto menu = menuBar()->addMenu( tr( "&File" ) );
  menu->addAction( newAct );
  menu->addAction( openAct );
  menu->addAction( saveAct );
  menu->addAction( saveAsAct );
  menu->addAction( mCloseAction );

  //----------------------------------------------------------
  mGripAction = new QAction( tr( "&Grips" ), this );
  mGripAction->setCheckable( true );
  mGripAction->setChecked( true );
  mGripAction->setShortcut( QKeySequence( Qt::CTRL + Qt::Key_G ) );
  mGripAction->setStatusTip( tr( "Show/hide grips" ) );
  connect( mGripAction, & QAction::changed, this, & MainWindow::gripActionChanged );

  auto viewMenu = menuBar()->addMenu( tr( "&View" ) );
  viewMenu->addAction( mGripAction );
}
//============================================================
void MainWindow::closeEvent(QCloseEvent* /*event*/)
{
  qApp->quit();
}
//============================================================
void MainWindow::newGuide()
{
  mControlWidget->addNewGuide();
  updateMenus();
  //Widget does not know new guide was created, so explicitly render them all.
  mGripWidget->update();
}
//============================================================
void MainWindow::closeGuide()
{
  mControlWidget->removeCurrentGuide();
  updateMenus();
}
//============================================================
void MainWindow::gripActionChanged()
{
  emit showGripsSignal( mGripAction->isChecked() );
}
//============================================================
void MainWindow::setControlWidget(ControlWidget* w)
{
  mControlWidget = w;
  setCentralWidget( w );
}
//============================================================
void MainWindow::openFile()
{
  mFile->open();
  mControlWidget->loadRegistry();
}
//============================================================
void MainWindow::saveFile()
{
  mFile->save();
}
//============================================================
void MainWindow::saveAsFile()
{
  mFile->saveAs();
}
//============================================================
void MainWindow::setGripWidget(GripWidget* grip)
{
  mGripWidget = grip;
}
//============================================================
void MainWindow::updateMenus()
{
  mCloseAction->setEnabled( GuideRegistry::instance()->getMap().count() > 1 );
}
//============================================================
