#include <QApplication>
#include <QCheckBox>
#include <QColorDialog>
#include <QComboBox>
#include <QDebug>
#include <QDesktopWidget>
#include <QGridLayout>
#include <QKeyEvent>
#include <QLabel>
#include <QPushButton>
#include <QStyle>

#include "controlWidget.h"
#include "guideRegistry.h"
#include "guideWidget.h"
#include "spinBox.h"

const QString COLOR_STYLE("QPushButton { background-color : %1; }");

//============================================================
ControlWidget::ControlWidget(GuideWidget* guide, QWidget* parent) :
  QWidget( parent ),
  mGuideWidget( guide )
{
  init();
}
//============================================================
ControlWidget::~ControlWidget()
{
}
//============================================================
void ControlWidget::init()
{
  mRegistry = GuideRegistry::instance();
  initWidgets();
  initLayout();
  initSignals();
}
//============================================================
void ControlWidget::initDefaults()
{
  QDesktopWidget desktop;
  auto center = desktop.screenGeometry( desktop.primaryScreen() ).center();
  mBottom->setValue( center.y() + 199 );
  mLeft->setValue( center.x() - 200 );
  mRight->setValue( center.x() + 199 );
  mTop->setValue( center.y() - 200 );
  setColor( QColor( rand() % 255, rand() % 255, rand() % 255 ) );
  mOpacity->setValue( 255 );
  mDash->setValue( 10 );
  mDim->setValue( 255 );
  mVisible->setChecked( true );
}
//============================================================
void ControlWidget::initWidgets()
{
  mIds = new QComboBox;

  mBottom = new SpinBox;
  mBottom->setRange( 0, 1e4 );

  mDash = new SpinBox;
  mDash->setRange( 0, 100 );

  mDim = new SpinBox;
  mDim->setRange( 0, 255 );

  mHeight = new SpinBox;
  mHeight->setRange( -1e4, 1e4 );

  mLeft = new SpinBox;
  mLeft->setRange( 0, 1e4 );

  mOpacity = new SpinBox;
  mOpacity->setRange( 0, 255 );
  mOpacity->setValue( 255 );

  mRight = new SpinBox;
  mRight->setRange( 0, 1e4 );

  mTop = new SpinBox;
  mTop->setRange( 0, 1e4 );

  mWidth = new SpinBox;
  mWidth->setRange( -1e4, 1e4 );

  mColor = new QPushButton;
  mColor->setMaximumWidth( 25 );
  setColor( Qt::black );

  mVisible = new QCheckBox;
}
//============================================================
void ControlWidget::initLayout()
{
  auto id     = new QLabel( "Id:" );
  auto top    = new QLabel( "Top:" );
  auto left   = new QLabel( "Left:" );
  auto right  = new QLabel( "Right:" );
  auto bottom = new QLabel( "Bottom:" );
  auto width  = new QLabel( "Width:" );
  auto height = new QLabel( "Height:" );
  auto color  = new QLabel( "Color:" );
  auto opacity= new QLabel( "Opacity:" );
  auto dash   = new QLabel( "Dash:" );
  auto dim    = new QLabel( "Dim:" );
  auto vis    = new QLabel( "Visible:" );

  auto colorContainer = new QWidget;
  {
    auto grid = new QGridLayout( colorContainer );
    grid->setMargin( 0 );
    grid->setSpacing( 0 );
    grid->setColumnMinimumWidth( 0,
                                 style()->pixelMetric( QStyle::PM_FocusFrameHMargin ) / 2 );
    grid->addWidget( mColor, 0,1, Qt::AlignLeft );
  }

  int r = 0;
  auto grid = new QGridLayout( this );
  grid->addWidget( id,      r,0 );
  grid->addWidget( mIds,    r,1 );
  grid->addWidget( top,   ++r,0 );
  grid->addWidget( mTop,    r,1 );
  grid->addWidget( left,  ++r,0 );
  grid->addWidget( mLeft,   r,1 );
  grid->addWidget( right, ++r,0 );
  grid->addWidget( mRight,    r,1 );
  grid->addWidget( bottom,  ++r,0 );
  grid->addWidget( mBottom,   r,1 );
  grid->addWidget( width,   ++r,0 );
  grid->addWidget( mWidth,    r,1 );
  grid->addWidget( height,  ++r,0 );
  grid->addWidget( mHeight,   r,1 );
  grid->addWidget( color,          ++r,0 );
  grid->addWidget( colorContainer,   r,1 );
  grid->addWidget( opacity,  ++r,0 );
  grid->addWidget( mOpacity,   r,1 );
  grid->addWidget( dash,     ++r,0 );
  grid->addWidget( mDash,      r,1 );
  grid->addWidget( dim,      ++r,0 );
  grid->addWidget( mDim,       r,1 );
  grid->addWidget( vis,      ++r,0 );
  grid->addWidget( mVisible,   r,1 );
}
//============================================================
void ControlWidget::initSignals()
{
  connect( mBottom, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
           this, & ControlWidget::bottomValueChangedSlot );
  connect( mColor, & QPushButton::clicked, this, & ControlWidget::colorClicked );
  connect( mDash, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
           this, & ControlWidget::dashValueChangedSlot );
  connect( mDim, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
           this, & ControlWidget::dimValueChangedSlot );
  connect( mHeight, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
           this, & ControlWidget::heightValueChangedSlot );
  connect( mIds, & QComboBox::currentTextChanged,
           this, & ControlWidget::idValueChangedSlot );
  connect( mLeft, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
           this, & ControlWidget::leftValueChangedSlot );
  connect( mOpacity, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
           this, & ControlWidget::opacityValueChangedSlot );
  connect( mRight, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
           this, & ControlWidget::rightValueChangedSlot );
  connect( mTop, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
           this, & ControlWidget::topValueChangedSlot );
  connect( mWidth, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
           this, & ControlWidget::widthValueChangedSlot );

  connect( mGuideWidget, & GuideWidget::dimChangedSignal,
           mDim,   & QSpinBox::setValue );
  connect( mVisible, & QCheckBox::stateChanged,
           this,     & ControlWidget::visibleChangedSlot );
}
//============================================================
void ControlWidget::keyPressEvent(QKeyEvent *event)
{
  QWidget::keyPressEvent( event );
}
//============================================================
void ControlWidget::addNewGuide()
{
  auto g = mRegistry->add();
  setCurrentGuide( g->id, false );
  initDefaults();
}
//============================================================
void ControlWidget::bottomValueChangedSlot(int v)
{
  if( auto g = currentGuide() )
  {
    g->bottom = v;
    mGuideWidget->update();
    mHeight->setValue( getHeight() );
    emitGuideChangedSignal();
  }
}
//============================================================
Guide* ControlWidget::currentGuide() const
{
  return mRegistry->get( currentGuideId() );
}
//============================================================
int ControlWidget::currentGuideId() const
{
  if( 0 == mIds->count() )
    return -1;

  int id = mIds->currentData().toInt();

  return id;
}
//============================================================
void ControlWidget::dashValueChangedSlot(int v)
{
  if( auto g = currentGuide() )
  {
    g->dash = v;
    mGuideWidget->update();
  }
}
//============================================================
void ControlWidget::dimValueChangedSlot(int v)
{
  if( auto g = currentGuide() )
  {
    g->dim = v;
    mGuideWidget->update();
  }
}
//============================================================
void ControlWidget::emitGuideChangedSignal()
{
  emit guideChangedSignal( currentGuideId() );
}
//============================================================
int ControlWidget::getHeight() const
{
  if( auto g = currentGuide() )
    return g->bottom - g->top + 1;

  return 0;
}
//============================================================
int ControlWidget::getWidth() const
{
  if( auto g = currentGuide() )
    return g->right - g->left + 1;

  return 0;
}
//============================================================
void ControlWidget::guideMovedSlot(int id)
{
  if( currentGuideId() != id )
    return;

  auto g = mRegistry->get( id );

  mBottom->blockSignals( true );
  mBottom->setValue( g->bottom );
  mBottom->blockSignals( false );

  mLeft->blockSignals( true );
  mLeft->setValue( g->left );
  mLeft->blockSignals( false );

  mRight->blockSignals( true );
  mRight->setValue( g->right );
  mRight->blockSignals( false );

  mTop->blockSignals( true );
  mTop->setValue( g->top );
  mTop->blockSignals( false);
}
//============================================================
void ControlWidget::guideResizedSlot(int id)
{
  if( currentGuideId() != id )
    return;

  auto g = mRegistry->get( id );

  mBottom->blockSignals( true );
  mBottom->setValue( g->bottom );
  mBottom->blockSignals( false );

  mRight->blockSignals( true );
  mRight->setValue( g->right );
  mRight->blockSignals( false );

  mWidth->blockSignals( true );
  mWidth->setValue( g->width() );
  mWidth->blockSignals( false );

  mHeight->blockSignals( true );
  mHeight->setValue( g->height() );
  mHeight->blockSignals( false );
}
//============================================================
void ControlWidget::heightValueChangedSlot(int v)
{
  if( auto g = currentGuide() )
  {
    g->bottom = g->top + v - 1;
    mGuideWidget->update();
    emitGuideChangedSignal();
  }
}
//============================================================
void ControlWidget::idValueChangedSlot(const QString &)
{
  restoreWidgets();
}
//============================================================
void ControlWidget::leftValueChangedSlot(int v)
{
  if( auto g = currentGuide() )
  {
    g->left = v;
    mWidth->setValue( getWidth() );
    mGuideWidget->update();
    emitGuideChangedSignal();
  }
}
//============================================================
void ControlWidget::loadRegistry()
{
  mIds->clear();

  auto map = mRegistry->getMap();
  auto end = map.end();
  for( auto iter = map.begin(); iter != end; ++iter )
  {
    int id = iter.key();
    mIds->addItem( QString::number( id ), id );
  }

  restoreWidgets();
}
//============================================================
void ControlWidget::removeCurrentGuide()
{
  int id = currentGuideId();
  if( id < 0 )
    return;

  mRegistry->remove( id );

  int index = mIds->findData( id );
  if( index >= 0 )
  {
    mIds->removeItem( index );
  }

  id = mRegistry->lastId();
  setCurrentGuide( id, true );
}
//============================================================
void ControlWidget::restoreWidgets()
{
  int id = currentGuideId();
  if( id < 0 )
    return;

  auto g = mRegistry->get( id );
  mBottom->setValue( g->bottom );
  mLeft->setValue( g->left );
  mRight->setValue( g->right );
  mTop->setValue( g->top );
  setColor( g->color );
  mOpacity->setValue( g->opacity );
  mDash->setValue( g->dash );
  mDim->setValue( g->dim );
  mVisible->setChecked( g->visible );
}
//============================================================
void ControlWidget::rightValueChangedSlot(int v)
{
  if( auto g = currentGuide() )
  {
    g->right = v;
    mWidth->setValue( getWidth() );
    mGuideWidget->update();
    emitGuideChangedSignal();
  }
}
//============================================================
void ControlWidget::setCurrentGuide(int id, bool updateWidgets)
{
  auto item = mRegistry->get( id );
  if( !item )
    return;

  int index = mIds->findData( id );
  if( index < 0 )
  {
    mIds->addItem( QString::number( id ), id );
    index = mIds->count() - 1;
  }

  mIds->setCurrentIndex( index );
  if( updateWidgets )
  {
    restoreWidgets();
  }

  mGuideWidget->update();
}
//============================================================
void ControlWidget::topValueChangedSlot(int v)
{
  if( auto g = currentGuide() )
  {
    g->top = v;
    mHeight->setValue( getHeight() );
    mGuideWidget->update();
    emitGuideChangedSignal();
  }
}
//============================================================
void ControlWidget::visibleChangedSlot(int v)
{
  if( auto g = currentGuide() )
  {
    g->visible = v;
    mGuideWidget->update();
  }
}
//============================================================
void ControlWidget::widthValueChangedSlot(int v)
{
  if( auto g = currentGuide() )
  {
    g->right = g->left + v - 1;
    mWidth->setValue( getWidth() );
    mGuideWidget->update();
    emitGuideChangedSignal();
  }
}
//============================================================
void ControlWidget::opacityValueChangedSlot(int v)
{
  if( auto g = currentGuide() )
  {
    g->opacity = v;
    mGuideWidget->update();
  }
}
//============================================================
void ControlWidget::colorClicked()
{
  const QColor color = QColorDialog::getColor( Qt::black, this, "Select Color" );
  if( !color.isValid() )
    return;

  setColor( color );
}
//============================================================
void ControlWidget::setColor(QColor c)
{
  auto text = QString( "rgb(%1,%2,%3)" )
              .arg( c.red() )
              .arg( c.green() )
              .arg( c.blue() );

  mColor->setStyleSheet( COLOR_STYLE.arg( text ) );

  if( auto g = currentGuide() )
  {
    g->color = c;
    mGuideWidget->update();
  }
}
//============================================================
