#pragma once

#include <QVariantMap>

class Guide;

class GuideEncoder
{
public:
  static void decode(const QVariantMap & json);

  static QVariantMap encode();
  static QVariantMap encodeGuide(const Guide* guide);
};
