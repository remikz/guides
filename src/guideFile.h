#pragma once

#include <QString>

class GuideFile
{
public:
  void open();
  void save();
  void saveAs();

private:
  QString mUrl;
};
