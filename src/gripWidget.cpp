#include <QDebug>
#include <QMouseEvent>
#include <QPainter>
#include <QtCore>

#include "gripWidget.h"
#include "guide.h"
#include "guideRegistry.h"
#include "guideWidget.h"

#define SIZE 35

//============================================================
GripWidget::GripWidget(GuideWidget* guide,
                       QWidget* parent) :
  QWidget( parent ),
  mGuideWidget( guide ),
  mIsDragging( false ),
  mPressedRegionIndex( -1 )
{
  init();
}
//============================================================
void GripWidget::guideChangedSlot(int)
{
  update();
}
//============================================================
void GripWidget::init()
{
  mRegistry = GuideRegistry::instance();
  setMouseTracking( true );
  initCursor();
}
//============================================================
void GripWidget::initCursor()
{
  QPixmap pixmap( 32, 32 );
  pixmap.fill( Qt::transparent );
  mMoveCursor = QCursor( pixmap );
}
//============================================================
void GripWidget::enterEvent(QEvent *event)
{
  QWidget::enterEvent( event );
}
//============================================================
void GripWidget::leaveEvent(QEvent *event)
{
  QWidget::leaveEvent( event );
}
//============================================================
void GripWidget::mouseMoveEvent(QMouseEvent *event)
{
  if( mIsDragging )
  {
    if( mPressedRegionIndex >= 0 &&
        mPressedRegionIndex < mRegions.count() )
    {
      mCurPt = event->pos();

      auto deltaPt = mCurPt - mKeyPt;
      if( event->modifiers() & Qt::ControlModifier )
      {
        deltaPt.rx() = 0;
      }
      else if( event->modifiers() & Qt::AltModifier )
      {
        deltaPt.ry() = 0;
      }

      if( event->modifiers() & Qt::ShiftModifier )
      {
        mGranularPt.rx() += .01 * deltaPt.x();
        deltaPt.rx() = 0;
        if( mGranularPt.x() > 1 )
        {
          deltaPt.rx() = 1;
          mGranularPt.rx() = 0;
        }
        else if( mGranularPt.x() < -1 )
        {
          deltaPt.rx() = -1;
          mGranularPt.rx() = 0;
        }

        mGranularPt.ry() += .01 * deltaPt.y();
        deltaPt.ry() = 0;
        if( mGranularPt.y() > 1 )
        {
          deltaPt.ry() = 1;
          mGranularPt.ry() = 0;
        }
        else if( mGranularPt.y() < -1 )
        {
          deltaPt.ry() = -1;
          mGranularPt.ry() = 0;
        }
      }
      else
      {
        mGranularPt.rx() = mGranularPt.ry() = 0;
      }

      const auto & region = mRegions.at( mPressedRegionIndex );
      int id = region.id;
      auto guide = mRegistry->get( id );

      //Store current rects that need to be erased.
      auto repaintGuideRects = guide->region();
      auto repaintGridRects = regions( guide );

      //Compute new guide.
      //Move.
      if( region.topLeft )
      {
        guide->bottom += deltaPt.y();
        guide->top    += deltaPt.y();
        guide->left   += deltaPt.x();
        guide->right  += deltaPt.x();
        emit guideMovedSignal( id );
      }
      //Resize.
      else
      {
        guide->bottom += deltaPt.y();
        guide->right  += deltaPt.x();
        emit guideResizedSignal( id );
      }

      mKeyPt = mCurPt;

      //To-be painted rects.
      repaintGuideRects += guide->region();
      repaintGridRects  += regions( guide );

      mGuideWidget->update( repaintGuideRects );
      update( repaintGridRects );
    }
  }

  QWidget::mouseMoveEvent( event );
}
//============================================================
void GripWidget::mousePressEvent(QMouseEvent *event)
{
  mKeyPt = mPressPt = event->pos();
  mGranularPt = QPointF( 0, 0 );
  mIsDragging = true;
  setCursor( mMoveCursor );
  updateRegionIndexPressed();
  QWidget::mousePressEvent( event );
}
//============================================================
void GripWidget::mouseReleaseEvent(QMouseEvent *event)
{
  mCurPt = mKeyPt = mPressPt = QPoint();
  mIsDragging = false;
  mPressedRegionIndex = -1;
  setCursor( QCursor() );
  QWidget::mouseReleaseEvent( event );
}
//============================================================
void GripWidget::paintEvent(QPaintEvent* /*event*/)
{
  const auto & map = mRegistry->getMap();
  const auto end   = map.end();

  QPainter painter( this );

  mRegions.clear();

  for( auto iter = map.begin(); iter != end; ++iter )
  {
    auto guide = iter.value();
    paintGrip( painter, guide );
  }
}
//============================================================
void GripWidget::paintGrip(QPainter & painter, Guide* guide)
{
  auto text = QString::number( guide->left );
  text.append( '\n' );
  text.append( QString::number( guide->top ) );

  auto rect = topLeftRect( guide );
  painter.fillRect( rect, Qt::white );

  //Drop shadow text does not look nice, so don't bother
  painter.setPen( Qt::black );
  painter.drawText( rect, Qt::AlignCenter, text );

  mRegions << Region{ true, guide->id, rect };

  //----------------------------------------------------------
  text = QString::number( guide->width() );
  text.append( '\n' );
  text.append( QString::number( guide->height() ) );

  rect = bottomRightRect( guide );
  painter.fillRect( rect, Qt::white );

  painter.setPen( Qt::black );
  painter.drawText( rect, Qt::AlignCenter, text );

  mRegions << Region{ false, guide->id, rect };
}
//============================================================
QRegion GripWidget::regions(Guide* guide) const
{
  QRegion result( topLeftRect( guide ) );
  result += bottomRightRect( guide );

  return result;
}
//============================================================
QRect GripWidget::topLeftRect(Guide* guide) const
{
  return QRect( guide->left - SIZE, guide->top - SIZE, SIZE, SIZE );
}
//============================================================
QRect GripWidget::bottomRightRect(Guide* guide) const
{
  return QRect( guide->right, guide->bottom, SIZE, SIZE );
}
//============================================================
void GripWidget::updateRegionIndexPressed()
{
  int i = 0;
  for( const auto & region: mRegions )
  {
    if( region.rect.contains( mPressPt ) )
    {
      mPressedRegionIndex = i;
      return;
    }
    ++i;
  }

  mPressedRegionIndex = -1;
}
//============================================================
