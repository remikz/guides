HEADERS+= \
          $$PWD/controlWidget.h \
          $$PWD/gripWidget.h \
          $$PWD/gripWindow.h \
          $$PWD/guide.h \
          $$PWD/guideEncoder.h \
          $$PWD/guideFile.h \
          $$PWD/guideRegistry.h \
          $$PWD/guideWidget.h \
          $$PWD/guideWindow.h \
          $$PWD/mainWindow.h \
          $$PWD/spinBox.h \

SOURCES+= \
          $$PWD/controlWidget.cpp \
          $$PWD/gripWidget.cpp \
          $$PWD/gripWindow.cpp \
          $$PWD/guide.cpp \
          $$PWD/guideEncoder.cpp \
          $$PWD/guideFile.cpp \
          $$PWD/guideRegistry.cpp \
          $$PWD/guideWidget.cpp \
          $$PWD/guideWindow.cpp \
          $$PWD/main.cpp \
          $$PWD/mainWindow.cpp \
          $$PWD/spinBox.cpp \

