#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>

#include "guideEncoder.h"
#include "guideFile.h"

//============================================================
void GuideFile::open()
{
  auto url = QFileDialog::getOpenFileName();
  if( url.isEmpty() )
    return;

  mUrl = url;
  QFile file( mUrl );
  QByteArray bytes;
  if( file.open( QFile::ReadOnly ) )
  {
    bytes = file.readAll();
    file.close();
  }

  QJsonParseError *error = nullptr;
  auto doc = QJsonDocument::fromJson( bytes, error );

  if( error )
  {
    QMessageBox::critical( nullptr, "File Error", "Failed to open file" );
    return;
  }

  auto map = doc.object().toVariantMap();
  GuideEncoder::decode( map );
}
//============================================================
void GuideFile::save()
{
  if( mUrl.isEmpty() )
  {
    saveAs();
    return;
  }

  auto map   = GuideEncoder::encode();
  auto obj   = QJsonObject::fromVariantMap( map );
  auto bytes = QJsonDocument( obj ).toJson( QJsonDocument::Indented );

  QFile file( mUrl );
  if( file.open( QFile::WriteOnly ) )
  {
    file.write( bytes );
    file.close();
  }
}
//============================================================
void GuideFile::saveAs()
{
  auto url = QFileDialog::getSaveFileName();
  if( url.isEmpty() )
    return;

  mUrl = url;
  save();
}
//============================================================
