#include <QDebug>
#include <QGridLayout>
#include <QLabel>
#include <QKeyEvent>
#include <QPushButton>

#include <QDesktopWidget>

#include "gripWidget.h"
#include "gripWindow.h"

//============================================================
GripWindow::GripWindow(GripWidget* grip,
                       QWidget *parent) :
  QMainWindow( parent ),
  mGrip( grip )
{
  setWindowFlags( Qt::FramelessWindowHint
                  | Qt::WindowStaysOnTopHint
                  | Qt::NoDropShadowWindowHint
                );
  setAttribute( Qt::WA_NoSystemBackground, true );
  setAttribute( Qt::WA_TranslucentBackground, true );
  setFocusPolicy( Qt::NoFocus );

  move( 0, 0 );
  QDesktopWidget desktop;
  resize( desktop.screenGeometry( desktop.screenNumber( this ) ).size() );

  auto container = new QWidget;
  auto grid = new QGridLayout( container );
  grid->setSpacing( 0 );
  grid->setMargin( 0 );
  grid->addWidget( grip );
  setCentralWidget( container );
}
//============================================================
