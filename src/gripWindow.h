#pragma once

#include <QMainWindow>

class GripWidget;

class GripWindow : public QMainWindow
{
  Q_OBJECT
public:
  GripWindow(GripWidget* grip,
             QWidget *parent=nullptr);

private:
  GripWidget* mGrip;
};
