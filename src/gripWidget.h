#pragma once

#include <QWidget>

class Guide;
class GuideRegistry;
class GuideWidget;

class GripWidget : public QWidget
{
  Q_OBJECT
public:
  GripWidget(GuideWidget* guide,
             QWidget* parent=nullptr);

public slots:
  void guideChangedSlot(int id);

signals:
  void guideMovedSignal(int id);
  void guideResizedSignal(int id);

protected:
  void init();
  void initCursor();

  void enterEvent(QEvent *event) override;
  void leaveEvent(QEvent *event) override;

  void mouseMoveEvent(QMouseEvent *event) override;
  void mousePressEvent(QMouseEvent *event) override;
  void mouseReleaseEvent(QMouseEvent *event) override;

  void paintEvent(QPaintEvent *event) override;
  void paintGrip(QPainter & painter, Guide* guide);

  QRegion regions(Guide* guide) const;
  QRect topLeftRect(Guide* guide) const;
  QRect bottomRightRect(Guide* guide) const;

  void updateRegionIndexPressed();

private:
  struct Region
  {
    bool topLeft;
    int id;
    QRect rect;
  };

  QPoint mCurPt, mKeyPt, mPressPt;
  QPointF mGranularPt;
  GuideWidget* mGuideWidget;
  bool mIsDragging;
  QCursor mMoveCursor;
  int mPressedRegionIndex;
  QList<Region> mRegions;
  GuideRegistry* mRegistry;
};
