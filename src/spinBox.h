#pragma once

#include <QSpinBox>

class SpinBox : public QSpinBox
{
  Q_OBJECT
public:

protected:
  void keyPressEvent(QKeyEvent *event) override;
};
